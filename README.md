# DEVOTEAM - RUST ROCKET HELLO WORLD

## Build and run container image

```bash
podman machine init \
    --volume /Users:/Users

podman build \
    --tag rust-rocket-hello-world \
    /Users/tdesaules/Repositories/Devoteam/GitLab/rust-rocket-hello-world/.container

podman run \
    --detach \
    --interactive \
    --tty \
    --name rust-rocket-hello-world \
    --publish 8000:8000 \
    --mount type=bind,source=/Users/tdesaules/Repositories/Devoteam/GitLab/rust-rocket-hello-world,target=/workspace rust-rocket-hello-world \
    sleep infinity

docker exec \
    --interactive \
    --tty rust-rocket-hello-world \
    bash
```

## Configure & Run

```bash
cargo run
```

## Contributing

**Product Owner :**
- [Thibault DESAULE](mailto:thibault@desaules.me)

**Scrum Master :**
- [Thibault DESAULE](mailto:thibault@desaules.me)

**Development team :**
- [Thibault DESAULE](mailto:thibault@desaules.me)

***
